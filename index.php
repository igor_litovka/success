<?php 
/**
*  parent Class
*/
class Person {
	
	public $ability = "Imagination";
	public $verb = "is";
	const END_OF_SENTENCE = "more important than knowledge";

	protected $persone_name;

	function viewQuote() {
		echo $this->persone_quote . '. ' . $this->persone_name . '<br/>'; 
	}

}

/**
*  outstanding Class 
*/
class GreatPerson extends Person
{
	const PERSON = "Albert Einstein";

	function __construct() {
		$this->persone_name = self::PERSON;
		$this->setQuoteText();
	}

	function setQuoteText() {
		$this->persone_quote = $this->ability . ' ' . $this->verb . ' '. self::END_OF_SENTENCE;
	}

}

/**
* Class for other people who want to be outstanding
*/
class OtherPerson extends Person
{

	function __construct($name = false, $extra_ability = false)
	{
		$this->persone_name = $name;
		$this->extra_ability = $extra_ability;
		$this->checkExtraAbility();
		$this->setQuoteText();
	}

	function checkExtraAbility () {
		if (!empty($this->extra_ability)) {
			$this->verb = "are";
			$this->union = "and";
			$this->space = " ";
		}
		else {
			$this->union = false;
			$this->space = false;	
		}
	}

	function setQuoteText() {
		$this->persone_quote = $this->ability . $this->space . $this->union . $this->space . $this->extra_ability .' ' . $this->verb . ' '. self::END_OF_SENTENCE;
	}

}


$albert = new GreatPerson();
$albert->viewQuote();

$igor = new OtherPerson( ' Igor Litovka ', ' desire to learn ' );
$igor->viewQuote();

?>